### 1. VCS -Version Control System    
1. Central VCS (SVN)  
2. Distributed VCS(GIT)  
~~~
	The concept of a centralized system is that it works on a Client-Server relationship. The repository is located at one place and provides access to many clients.  
In a Distributed System, every user has a local copy of the repository in addition to the central repo on the server side.  
~~~


### 2. Installing GIT  
~~~
Go to https://git-scm.com/  download and install on your system. It is very simple.  
~~~

### 3. First Time SetUp

**3.1:Check VERSION**
~~~
$ git --version  
git version 2.16.3  
~~~

**3.2:Set Config Values**     
~~~
$ git config --global user.name "Ashish"
$ git config --global user.email "ashish123456789@gmail.com"
$ git config --list
~~~


### 4. Need Help
~~~
git help <verb> or git <verb> --help 
eg:
$ git help config
or
$ git config --help
This will be helpful to check the manual when doing advanced stuffs  
~~~

### 5. Getting started  
Two common scenarios.  
1. An Existing project in local machine that you want to be tracked by GIT  
2. An Existing project existing remotely that you want to start developing on.  

**5.1 Initilize a repo from existing code in the local machine**  
~~~
$ git init  
This command will create a .git directory inside our project which contais all the info related to our repo.  
If ever we want to stop tracking our project with git, just delete this folder.  
~~~

**5.2 Before first Commit**
~~~
Before we commit anything, run the git status command.  
$ git status
If we want to ignore files by git(which are related to personal preference etc like .project) create a .gitignore file  
$ touch .gitignore  
It is a simple text file where we can add files that we want git to ignore.
sample content

.project
.blabla
.DS_Store
*.class
*.pyc

After adding the required contents inside the gitignore file when we check git status the ignored files will not be visible. 
~~~

  
### 6. Where are we now?
~~~
We have 3 areas . 
1)Working Directory
2)Staging Area . 
3) ".git directory"(Repository)

Untracked and modified files will be in the WD and we can list those when we run git status command.  
Staging area is where we organise what we want to be commited to the repository.  
~~~

### 7. Add files to Staging area
~~~
We can add all files that are currently untracked or that we made changes to to the staging area using 
$ git add -A
or we can add files individualy as  
$ git add abc.java
After that we can check the status as 
$ git status  
~~~

### 8. Remove files from Staging area  
~~~
$ git reset   
This will move all the staging files to Working directory as untracked files
$ git reset abc.java    
This will move only the abc.java file to the working directory.  
~~~

### 9. First commit  
~~~
$ git add -A
$ git status
$ git commit -m  "Initial Commit"
$ git status
$ git log
~~~

### 10. Cloning a remote repo  
~~~
$ git clone <url> <where to clone>
"$ git clone https://github.com/Abcrepo/remote_repo.git ."
The .(dot) means clone all the files from that repo to the current directory
~~~

### 10. Viewing info about the remote repo which we cloned
~~~
$ git remote -v
$ git branch -a
~~~

### 11. Pushing changes
~~~
Commit changes and then push.  

$ git diff
$ git status
$ git add -A
$ git commit -m "Modified abc function"

$ git pull origin master
$ git push origin master

~~~

### 12. Common workflow
~~~
$ git branch calc-divide (this will create a new branch)
$ git branch  (this will list the branches)
$ git checkout calc-divide (this will switch to the new branch and we can start working on this branch)

make changes in the files

$ git diff
$ git status
$ git add -A
$ git commit -m "Modified abc function"

$ git push -u origin calc-divide  (-u means we want to associate our local calc-divide branch with remote calc-divide branch and 
in the future we can just do simply git pull and git push and It will know that those two brances are associated to eachother)

$ git branch -a
~~~

### 13. Merge a Branch
~~~
We need to check all the unit tests are running successfully and build is also fine, then we can merge that branch with Master(it depned on Company policy also on when to merge)

$ git checkout master
$ git pull origin master
$ git branch --merged (this will tell us the branches that we have merged-in so far)
$ git merge calc-divide (we are in master branch and now we have merged the calc-divide branch with master)
$ git push origin master (pushing the changes to master branch on the remote repo)
Now finished meging the calc-divide branch with master and we are ready to delete this calc-devide branch
~~~

### 14. Deleting a Branch
~~~
$ git branch --merged
$ git branch -d calc-divide (Now the branch has been deleted locally. We neeed to push this delete to remote)
$ git branch -a  (this will show that the calc divide branch has been deleted from local repo but not yet from remote)
$ git push origin --delete calc-divide
$ git branch -a  (now you can see remote branch also deleted)
~~~

### 15. Faster example
~~~
$ git branch subtract
$ git checkout subtract
make the changes in the file
$ git status
$ git add -A
$ git commit -m "subtract fun added"
$ git push -u origin subtract
Build is fine now it is ready to merge with master
$ git checkout master
$ git pull origin master
$ git merge subtract
$ git push origin master
~~~
